# gitlab-ci-duration-counter

## Installation
Download this script, make sure all package are present, and run with the following 

## Usage

```
usage: gitlab-ci-durations-verbose.py [-h] -t TOKEN -g GROUP -s START_DATE -e END_DATE

options:
  -h, --help            show this help message and exit
  -t TOKEN, --token TOKEN
                        GitLab API token
  -g GROUP, --group GROUP
                        GitLab group ID
  -s START_DATE, --start_date START_DATE
                        Start date in YYYY-MM-DD format
  -e END_DATE, --end_date END_DATE
                        End date in YYYY-MM-DD format
                        
```


Example :
`python3 gitlab-ci-durations.py -t 'glpat-ABC123_AccessToken' -g 1234_GroupID -s 2022-12-01 -e 2022-01-01`

```
$ python3 gitlab-ci-durations.py -t 'glpat-ABC123_AccessToken' -g 1234_GroupID -s 2022-12-01 -e 2022-01-01

2023-05-08 16:15:06,513 - INFO - YOUR_GITLAB_API_TOKEN: glpat-ABC123_AccessToken
2023-05-08 16:15:06,513 - INFO - YOUR_GROUP_ID: 1234_GroupID
2023-05-08 16:15:06,513 - INFO - Start Date: 2022-12-01 00:00:00
2023-05-08 16:15:06,513 - INFO - End Date: 2022-01-01 00:00:00
Project name, total duration, total queued duration
Testruns, 26.59 seconds, 3.73 seconds
Some Stack, 1075.65 seconds, 108187594.39 seconds
internal, 556.58 seconds, 29.53 seconds
SDK Prototype Demo, 0.00 seconds, 0.00 seconds
project-web-page, 266.61 seconds, 37.71 seconds
my-app, 5299.45 seconds, 445.98 seconds
Total duration for all projects in group and subgroups, 7224.88 seconds, 108188111.34 seconds
```

## Support
For support or question, please open an issue in this projet.

## Contributing
Open to contributions, feel free to open a merge request in this project.

## Authors and acknowledgment
@comeh 

## License
MIT Licence

<!-- ## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
-->


## note / duplicate
might be worth having a look to https://gitlab.com/arvidnl/gitlab-job-scraper/ that might perform the same