# usage: python3 gitlab-ci-durations.py -t 'glpat-ABC123_AccessToken' -g 1234_GroupID -s 2022-12-01 -e 2022-01-01
import requests
import datetime
import json
import argparse
import logging

# set up logging
logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', level=logging.INFO)

parser = argparse.ArgumentParser()
parser.add_argument('-t', '--token', help='GitLab API token', required=True)
parser.add_argument('-g', '--group', help='GitLab group ID', required=True)
parser.add_argument('-s', '--start_date', help='Start date in YYYY-MM-DD format', required=True)
parser.add_argument('-e', '--end_date', help='End date in YYYY-MM-DD format', required=True)
args = parser.parse_args()

YOUR_GITLAB_API_TOKEN = args.token
YOUR_GROUP_ID = args.group
start_date = datetime.datetime.strptime(args.start_date, '%Y-%m-%d')
end_date = datetime.datetime.strptime(args.end_date, '%Y-%m-%d')

GITLAB_URL = 'https://gitlab.com/api/v4'


logging.info(f'YOUR_GITLAB_API_TOKEN: {YOUR_GITLAB_API_TOKEN}')
logging.info(f'YOUR_GROUP_ID: {YOUR_GROUP_ID}')
logging.info(f'Start Date: {start_date}')
logging.info(f'End Date: {end_date}')

# set up variables
group_id = YOUR_GROUP_ID              # change to your GitLab group or subgroup ID
api_token = YOUR_GITLAB_API_TOKEN     # change to your GitLab API token
headers = {'PRIVATE-TOKEN': api_token}

# function to get projects for a given group or subgroup ID
def get_projects(group_id):
    url = f'{GITLAB_URL}/groups/{group_id}/projects'
    response = requests.get(url, headers=headers)
    projects = response.json()
    return projects

# function to sum up jobs duration and queued_duration for all CI Jobs of a given project
def get_project_durations(project_id):
    url = f'https://gitlab.com/api/v4/projects/{project_id}/jobs?created_after={start_date}&created_before={end_date}&per_page=100'
    project_duration = 0
    project_queued_duration = 0
    project_jobs = 0
    
    while url:
        response = requests.get(url, headers=headers)
        jobs = response.json()
        for job in jobs:
            project_jobs += 1
            
            duration = job['duration'] if job['duration'] is not None else 0
            project_duration += duration
            
            queued_duration = job['queued_duration'] if job['queued_duration'] is not None else 0
            project_queued_duration += queued_duration
        
        url = None
        if 'X-Next-Page' in response.headers:
            print('@', end='', flush=True)
            next_page = response.headers['X-Next-Page']
            if next_page:
                url = f'https://gitlab.com/api/v4/projects/{project_id}/jobs?created_after={start_date}&created_before={end_date}&per_page=100&page={next_page}'
    
    return project_duration, project_queued_duration, project_jobs

# function to recursively get projects for all subgroups of a given group ID
def get_all_projects(group_id):
    projects = []
    subgroups_url = f'{GITLAB_URL}/groups/{group_id}/subgroups'
    response = requests.get(subgroups_url, headers=headers)
    if response.status_code == 200:
        subgroups = response.json()
        for subgroup in subgroups:
            projects += get_all_projects(subgroup['id'])
        projects += get_projects(group_id)
    elif response.status_code == 403:
        logging.warning(f'User does not have permission to access subgroups in group {group_id}')
        projects += get_projects(group_id)
    else:
        logging.error(f'Error {response.status_code} encountered when accessing subgroups in group {group_id}')
    return projects

# get all projects for the given group ID, including subgroups
all_projects = get_all_projects(group_id)

# loop through each project and get CI job durations
total_duration = 0
total_queued_duration = 0
total_number_jobs = 0

print(f"Project name ({start_date} to {end_date}), sum of job durations, sum of queued durations, number of jobs")
for project in all_projects:
    project_duration = project_queued_duration = project_jobs = 0
    project_id = project['id']
    project_name = project['name']
    #if project_id != 3836952:
    #    continue
    project_duration, project_queued_duration, project_jobs = get_project_durations(project_id)
    total_duration += project_duration
    total_queued_duration += project_queued_duration
    total_number_jobs += project_jobs
    
    print(f'{project_name} ({project_id}), {project_duration:.2f} seconds, {project_queued_duration:.2f} seconds, {project_jobs} jobs')

print(f'Total duration for all projects in group and subgroups, {total_duration:.2f} seconds, {total_queued_duration:.2f} seconds, {total_number_jobs} jobs ')
